This is dockerfile for https://hub.docker.com/r/lukacamernik/php/

`7.x-fpm-xdebug` is for development server including XDEBUG

`7.x-fpm` should be as clean and lean as possible for Production server!

To start new build enter directory to build for and execute `docker build . -t lukacamernik/php:<tag>` and make sure to replace the tag with what you are building